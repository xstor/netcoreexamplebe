﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetCoreExampleBE.Models
{
    public class ApplicationUser : IdentityUser
    {
        public int OrganizationId { get; set; }
        public Organization Organization { get; set; }

        public DateTimeOffset CreatedAt { get; set; }
        public DateTimeOffset? UpdatedAt { get; set; }
        public string Name { get; set; }
    }
}
