﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NetCoreExampleBE.Models
{
    public class Organization: BaseDatabaseObject
    {

        [Required]
        public string Name { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public bool TrialVersionOwned { get; set; }
        public string Phone { get; set; }
        public string EmployerIdentificationNumber { get; set; }
        public string BankAccount { get; set; }
        public virtual List<ApplicationUser> Users { get; set; }
    }
}
