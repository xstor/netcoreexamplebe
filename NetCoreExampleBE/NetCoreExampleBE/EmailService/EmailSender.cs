﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace NetCoreExampleBE.EmailService
{
    public class EmailSender
    {
            public static string basePath = "https://www.spiderstudio.cz";
            public static string ressetPasswordPage = basePath + "/obnovit-heslo";
            public static void sendEmail(string email, string title, string body)
            {
                SmtpClient smtpClient = new SmtpClient("smtp.forpsi.com", 587);
                smtpClient.UseDefaultCredentials = false;
                smtpClient.EnableSsl = true;
                smtpClient.Credentials = new System.Net.NetworkCredential("info@spiderstudio.cz", "sft5_dwufb");
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                MailMessage mail = new MailMessage();
                //Setting From , To and CC
                mail.From = new MailAddress("info@spiderstudio.cz");
                mail.To.Add(email);
                mail.Subject = title;
                mail.Body = body;
                mail.IsBodyHtml = true;

                try
                {
                    smtpClient.Send(mail); //try catch  když tak talogovat a poslat email mě}
                }
                catch (Exception e)
                {
                    sendEmail("vojtastor32@gmail.com", "Chyba v posílání emailu", "Chyba v posílná emailu na email: " + email + ". " + e.Message + " " + e);
                }
            }
            public static string prepareBody(string header, string body)
            {
                string imgSrc = "https://www.spiderstudio.cz/assets/img/logoEmail.png";
                string result = String.Format(@"<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional //EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">

<html xmlns=""http://www.w3.org/1999/xhtml"" xmlns:o=""urn:schemas-microsoft-com:office:office"" xmlns:v=""urn:schemas-microsoft-com:vml"">
<head>
<!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]-->
<meta content=""text/html; charset=utf-8"" http-equiv=""Content-Type""/>
<meta content=""width=device-width"" name=""viewport""/>
<!--[if !mso]><!-->
<meta content=""IE=edge"" http-equiv=""X-UA-Compatible""/>
<!--<![endif]-->
<title></title>
<!--[if !mso]><!-->
<link href=""https://fonts.googleapis.com/css?family=Lato"" rel=""stylesheet"" type=""text/css""/>
<!--<![endif]-->
<style type=""text/css"">
		body {{
			margin: 0;
			padding: 0;
		}}

		table,
		td,
		tr {{
			vertical-align: top;
			border-collapse: collapse;
		}}

		* {{
			line-height: inherit;
		}}

		a[x-apple-data-detectors=true] {{
			color: inherit !important;
			text-decoration: none !important;
		}}
	</style>
<style id=""media-query"" type=""text/css"">
		@media (max-width: 640px) {{

			.block-grid,
			.col {{
				min-width: 320px !important;
				max-width: 100% !important;
				display: block !important;
			}}

			.block-grid {{
				width: 100% !important;
			}}

			.col {{
				width: 100% !important;
			}}

			.col>div {{
				margin: 0 auto;
			}}

			img.fullwidth,
			img.fullwidthOnMobile {{
				max-width: 100% !important;
			}}

			.no-stack .col {{
				min-width: 0 !important;
				display: table-cell !important;
			}}

			.no-stack.two-up .col {{
				width: 50% !important;
			}}

			.no-stack .col.num4 {{
				width: 33% !important;
			}}

			.no-stack .col.num8 {{
				width: 66% !important;
			}}

			.no-stack .col.num4 {{
				width: 33% !important;
			}}

			.no-stack .col.num3 {{
				width: 25% !important;
			}}

			.no-stack .col.num6 {{
				width: 50% !important;
			}}

			.no-stack .col.num9 {{
				width: 75% !important;
			}}

			.video-block {{
				max-width: none !important;
			}}

			.mobile_hide {{
				min-height: 0px;
				max-height: 0px;
				max-width: 0px;
				display: none;
				overflow: hidden;
				font-size: 0px;
			}}

			.desktop_hide {{
				display: block !important;
				max-height: none !important;
			}}
		}}
	</style>
</head>
<body class=""clean-body"" style=""margin: 0; padding: 0; -webkit-text-size-adjust: 100%; background-color: #FFFFFF;"">
<!--[if IE]><div class=""ie-browser""><![endif]-->
<table bgcolor=""#FFFFFF"" cellpadding=""0"" cellspacing=""0"" class=""nl-container"" role=""presentation"" style=""table-layout: fixed; vertical-align: top; min-width: 320px; Margin: 0 auto; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #FFFFFF; width: 100%;"" valign=""top"" width=""100%"">
<tbody>
<tr style=""vertical-align: top;"" valign=""top"">
<td style=""word-break: break-word; vertical-align: top;"" valign=""top"">
<!--[if (mso)|(IE)]><table width=""100%"" cellpadding=""0"" cellspacing=""0"" border=""0""><tr><td align=""center"" style=""background-color:#FFFFFF""><![endif]-->
<div style=""background-color:transparent;"">
<div class=""block-grid two-up"" style=""Margin: 0 auto; min-width: 320px; max-width: 620px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;"">
<div style=""border-collapse: collapse;display: table;width: 100%;background-color:transparent;"">
<!--[if (mso)|(IE)]><table width=""100%"" cellpadding=""0"" cellspacing=""0"" border=""0"" style=""background-color:transparent;""><tr><td align=""center""><table cellpadding=""0"" cellspacing=""0"" border=""0"" style=""width:620px""><tr class=""layout-full-width"" style=""background-color:transparent""><![endif]-->
<!--[if (mso)|(IE)]><td align=""center"" width=""310"" style=""background-color:transparent;width:310px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"" valign=""top""><table width=""100%"" cellpadding=""0"" cellspacing=""0"" border=""0""><tr><td style=""padding-right: 10px; padding-left: 10px; padding-top:5px; padding-bottom:5px;""><![endif]-->
<div class=""col num6"" style=""max-width: 320px; min-width: 310px; display: table-cell; vertical-align: top; width: 310px;"">
<div style=""width:100% !important;"">
<!--[if (!mso)&(!IE)]><!-->
<div style=""border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 10px; padding-left: 10px;"">
<!--<![endif]-->
<div align=""left"" class=""img-container left autowidth fullwidth"" style=""padding-right: 0px;padding-left: 0px;"">
<!--[if mso]><table width=""100%"" cellpadding=""0"" cellspacing=""0"" border=""0""><tr style=""line-height:0px""><td style=""padding-right: 0px;padding-left: 0px;"" align=""left""><![endif]-->
<div style=""font-size:1px;line-height:15px""> </div><img alt=""Image"" border=""0"" class=""left autowidth fullwidth"" src=""{0}""  style=""text-decoration: none; -ms-interpolation-mode: bicubic; border: 0; height: auto; width: 100%; max-width: 290px; display: block;"" title=""Image"" width=""290""/>
<div style=""font-size:1px;line-height:15px""> </div>
<!--[if mso]></td></tr></table><![endif]-->
</div>
<!--[if (!mso)&(!IE)]><!-->
</div>
<!--<![endif]-->
</div>
</div>
<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
<!--[if (mso)|(IE)]></td><td align=""center"" width=""310"" style=""background-color:transparent;width:310px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"" valign=""top""><table width=""100%"" cellpadding=""0"" cellspacing=""0"" border=""0""><tr><td style=""padding-right: 10px; padding-left: 10px; padding-top:5px; padding-bottom:5px;""><![endif]-->
<div class=""col num6"" style=""max-width: 320px; min-width: 310px; display: table-cell; vertical-align: top; width: 310px;"">
<div style=""width:100% !important;"">
<!--[if (!mso)&(!IE)]><!-->
<div style=""border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 10px; padding-left: 10px;"">
<!--<![endif]-->
<div></div>
<!--[if (!mso)&(!IE)]><!-->
</div>
<!--<![endif]-->
</div>
</div>
<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
</div>
</div>
</div>
<div style=""background-color:transparent;"">
<div class=""block-grid"" style=""Margin: 0 auto; min-width: 320px; max-width: 620px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;"">
<div style=""border-collapse: collapse;display: table;width: 100%;background-color:transparent;"">
<!--[if (mso)|(IE)]><table width=""100%"" cellpadding=""0"" cellspacing=""0"" border=""0"" style=""background-color:transparent;""><tr><td align=""center""><table cellpadding=""0"" cellspacing=""0"" border=""0"" style=""width:620px""><tr class=""layout-full-width"" style=""background-color:transparent""><![endif]-->
<!--[if (mso)|(IE)]><td align=""center"" width=""620"" style=""background-color:transparent;width:620px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"" valign=""top""><table width=""100%"" cellpadding=""0"" cellspacing=""0"" border=""0""><tr><td style=""padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;""><![endif]-->
<div class=""col num12"" style=""min-width: 320px; max-width: 620px; display: table-cell; vertical-align: top; width: 620px;"">
<div style=""width:100% !important;"">
<!--[if (!mso)&(!IE)]><!-->
<div style=""border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;"">
<!--<![endif]-->
<table border=""0"" cellpadding=""0"" cellspacing=""0"" class=""divider"" role=""presentation"" style=""table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"" valign=""top"" width=""100%"">
<tbody>
<tr style=""vertical-align: top;"" valign=""top"">
<td class=""divider_inner"" style=""word-break: break-word; vertical-align: top; min-width: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top: 10px; padding-right: 10px; padding-bottom: 15px; padding-left: 10px;"" valign=""top"">
<table align=""center"" border=""0"" cellpadding=""0"" cellspacing=""0"" class=""divider_content"" role=""presentation"" style=""table-layout: fixed; vertical-align: top; border-spacing: 0; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; border-top: 1px solid #222222;"" valign=""top"" width=""100%"">
<tbody>
<tr style=""vertical-align: top;"" valign=""top"">
<td style=""word-break: break-word; vertical-align: top; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"" valign=""top""><span></span></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<!--[if (!mso)&(!IE)]><!-->
</div>
<!--<![endif]-->
</div>
</div>
<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
</div>
</div>
</div>
<div style=""background-color:transparent;"">
<div class=""block-grid"" style=""Margin: 0 auto; min-width: 320px; max-width: 620px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: transparent;"">
<div style=""border-collapse: collapse;display: table;width: 100%;background-color:transparent;"">
<!--[if (mso)|(IE)]><table width=""100%"" cellpadding=""0"" cellspacing=""0"" border=""0"" style=""background-color:transparent;""><tr><td align=""center""><table cellpadding=""0"" cellspacing=""0"" border=""0"" style=""width:620px""><tr class=""layout-full-width"" style=""background-color:transparent""><![endif]-->
<!--[if (mso)|(IE)]><td align=""center"" width=""620"" style=""background-color:transparent;width:620px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"" valign=""top""><table width=""100%"" cellpadding=""0"" cellspacing=""0"" border=""0""><tr><td style=""padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;""><![endif]-->
<div class=""col num12"" style=""min-width: 320px; max-width: 620px; display: table-cell; vertical-align: top; width: 620px;"">
<div style=""width:100% !important;"">
<!--[if (!mso)&(!IE)]><!-->
<div style=""border-top:0px solid transparent; border-left:0px solid transparent; border-bottom:0px solid transparent; border-right:0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;"">
<!--<![endif]-->
<!--[if mso]><table width=""100%"" cellpadding=""0"" cellspacing=""0"" border=""0""><tr><td style=""padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 30px; font-family: Tahoma, Verdana, sans-serif""><![endif]-->
<div style=""color:#71777D;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;line-height:150%;padding-top:10px;padding-right:10px;padding-bottom:30px;padding-left:10px;"">
<div style=""font-size: 12px; line-height: 18px; font-family: 'Lato', Tahoma, Verdana, Segoe, sans-serif; color: #71777D;"">
<p style=""font-size: 14px; line-height: 21px; margin: 0;""><span style=""color: #000000; font-size: 14px; line-height: 21px;""><strong>{1}</strong></span>,</p>
<p style=""font-size: 14px; line-height: 21px; margin: 0;"">{2} </p>
</div>
</div>
<!--[if mso]></td></tr></table><![endif]-->
<!--[if (!mso)&(!IE)]><!-->
</div>
<!--<![endif]-->
</div>
</div>
<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
</div>
</div>
</div>
<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
</td>
</tr>
</tbody>
</table>
<!--[if (IE)]></div><![endif]-->
</body>
</html>", imgSrc, header, body);
                return result;
            }
        }

    }

