﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using NetCoreExampleBE.Dtos.In;
using NetCoreExampleBE.Dtos.Out;
using NetCoreExampleBE.EmailService;
using NetCoreExampleBE.Models;
using NetCoreExampleBE.Repository.IRepository;

namespace NetCoreExampleBE.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IOrganizationRepository _organizationRepository;
        private readonly ILogger _logger;
        private readonly ApplicationSettings _appSettings;

        public AccountController(
                    UserManager<ApplicationUser> userManager,
                    SignInManager<ApplicationUser> signInManager,
                    ILogger<AccountController> logger, IOrganizationRepository organizationRepository, IOptions<ApplicationSettings> appSettings)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _organizationRepository = organizationRepository;
            _appSettings = appSettings.Value;
        }

        /// <summary>
        /// Change info about logged user
        /// </summary>
        /// <param name="dtoInUserChangeInfo">New info that will be updated.</param>
        /// <returns></returns>
        [HttpPut("ChangeMyUserInfo")]
        [ProducesResponseType(204, Type = typeof(void))]
        public async Task<IActionResult> EditUserInfo(DtoInUserChangeInfo dtoInUserChangeInfo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var userId = User.Claims.First(c => c.Type == "UserID").Value;
            var user = await _userManager.FindByIdAsync(userId);
            user.Name = dtoInUserChangeInfo.FullName;
            await _userManager.UpdateAsync(user);
            return StatusCode(204);
        }
        /// <summary>
        /// Get logged user info
        /// </summary>
        /// <returns></returns>
        [HttpGet("MyUserInfo")]
        [ProducesResponseType(200, Type = typeof(DtoOutUserInfo))]
        public async Task<IActionResult> GetManageInfo()
        {
            var userId = User.Claims.First(c => c.Type == "UserID").Value;
            ApplicationUser user = await _userManager.FindByIdAsync(userId);

            if (user == null)
            {
                return null;
            }

            return Ok(new DtoOutUserInfo()
            {
                Email = user.Email,
                FullName = user.Name
            });
        }

        /// <summary>
        /// Forgot password will send email with url where you can set new password
        /// </summary>
        /// <param name="model"> email which will be used to send the url</param>
        /// <returns></returns>
        [HttpPost("ForgotPassword")]
        [AllowAnonymous]
        [ProducesResponseType(200, Type = typeof(void))]
        public async Task<IActionResult> ForgotPassword(DtoInEmail model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);
                if (user == null)
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return Ok();
                }

                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=532713
                // Send an email with this link               
                var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                code = HttpUtility.UrlEncode(code);
                string body = String.Format("Kliknutím {0} si nastavte nové heslo.", String.Format(@"<a href=""{0}?token={1}"">zde</a>", EmailSender.ressetPasswordPage, code));
                EmailSender.sendEmail(model.Email, "Resetování hesla do Vstupenkovače Města Libochovice", EmailSender.prepareBody("Resetování hesla do Vstupenkovače Města Libochovice", body));
                return Ok();
            }
            return Ok();
        }
        /// <summary>
        /// Reset you password from token that you get in email
        /// </summary>
        /// <param name="model">new password and token from email</param>
        /// <returns></returns>
        [HttpPost("RessetPassword")]
        [AllowAnonymous]
        [ProducesResponseType(200, Type = typeof(void))]
        public async Task<IActionResult> RessetPassword(DtoInForgotPassword model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.ResetPasswordAsync(await _userManager.FindByEmailAsync(model.email), model.token, model.newPassword);
                if (!user.Succeeded)
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return BadRequest();
                }
                return Ok();
            }
            return BadRequest();
        }
        /// <summary>
        /// Change password of curently logged user
        /// </summary>
        /// <param name="model">old and new password</param>
        /// <returns></returns>
        // POST api/Account/ChangePassword
        [HttpPost("ChangePassword")]
        [ProducesResponseType(200, Type = typeof(void))]
        public async Task<IActionResult> ChangePassword(DtoInChangePassword model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var userId = User.Claims.First(c => c.Type == "UserID").Value;
            IdentityResult result = await _userManager.ChangePasswordAsync(await _userManager.FindByIdAsync(userId), model.OldPassword,
                model.NewPassword);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }
        /// <summary>
        /// login method that will return token
        /// </summary>
        /// <param name="model"> sign in credentials</param>
        /// <returns></returns>
        [HttpPost("Login")]
        [AllowAnonymous]
        [ProducesResponseType(200,Type=typeof(DtoOutLogin))]
        //POST : /api/ApplicationUser/Login
        public async Task<IActionResult> Login(DtoInLogin model)
        {
            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user != null && await _userManager.CheckPasswordAsync(user, model.Password))
            {
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim("UserID",user.Id.ToString())
                    }),
                    Expires = DateTime.UtcNow.AddHours(8),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_appSettings.JWT_Secret)), SecurityAlgorithms.HmacSha256Signature)
                };
                var tokenHandler = new JwtSecurityTokenHandler();
                var securityToken = tokenHandler.CreateToken(tokenDescriptor);
                var token = tokenHandler.WriteToken(securityToken);
                return Ok(new DtoOutLogin() { Token = token, ExpiresIn = securityToken.ValidTo });
            }
            else
                return BadRequest(new { message = "Username or password is incorrect." });
        }
        /// <summary>
        /// Register new user to organization
        /// </summary>
        /// <param name="model">new user that will be added to organization</param>
        /// <returns></returns>
        [HttpPost("RegisterNewUser")]
        [ProducesResponseType(200, Type = typeof(void))]
        public async Task<IActionResult> RegisterNewUser(DtoInRegisterUser model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var userId = User.Claims.First(c => c.Type == "UserID").Value;
            var organization = this._organizationRepository.GetOrganizationByUserId(userId);
            var user = new ApplicationUser { UserName = model.Email, Email = model.Email, OrganizationId = organization.Id, Name = model.Name };
            var result = await _userManager.CreateAsync(user, model.Password);
            if (!result.Succeeded)
            {
                AddErrors(result);
                return BadRequest(ModelState);
            }
            return Ok();
        }


        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private IActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return StatusCode(500);
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError("", error.Description);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }
        #endregion
    }
}