﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NetCoreExampleBE.Dtos.In;
using NetCoreExampleBE.Dtos.Out;
using NetCoreExampleBE.Models;
using NetCoreExampleBE.Repository.IRepository;

namespace NetCoreExampleBE.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class OrganizationsController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IOrganizationRepository _organizationRepository;
        private readonly ILogger _logger;
        private readonly IMapper _mapper;
        public OrganizationsController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            ILogger<AccountController> logger, IOrganizationRepository organizationRepository, IMapper mapper)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _organizationRepository = organizationRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// Delete user in organiaztion
        /// </summary>
        /// <param name="model">Email for user you want to delete</param>
        /// <returns></returns>
        [HttpPost("DeleteUser")]
        [ProducesResponseType(200, Type = typeof(void))]
        public async Task<IActionResult> DeleteUser([FromBody]DtoInEmail model)
        {
            var userId = User.Claims.First(c => c.Type == "UserID").Value;
            var me = await _userManager.FindByIdAsync(userId);
            if (me.Email == model.Email)
            {
                return BadRequest("Nemůžeš smazat sám sebe");
            }

            if (!this._organizationRepository.UserWithEmailExistsInOrganization(model.Email, this._organizationRepository.GetOrganizationByUserId(userId).Id))
            {
                return NotFound();
            }
            var user = await _userManager.FindByEmailAsync(model.Email);
            IdentityResult result = await _userManager.DeleteAsync(user);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }


        /// <summary>
        /// Change info about user in organization
        /// </summary>
        /// <param name="model">User info which will be updated</param>
        /// <returns></returns>
        [HttpPost("ChangeUserInfo")]
        [ProducesResponseType(200, Type = typeof(void))]
        public async Task<IActionResult> ChangeUserInfoAsAdmin(DtoInUserChangeInfo model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var userId = User.Claims.First(c => c.Type == "UserID").Value;
            if (!this._organizationRepository.UserWithEmailExistsInOrganization(model.Email, this._organizationRepository.GetOrganizationByUserId(userId).Id))
            {
                return NotFound();
            }
            var user = await _userManager.FindByEmailAsync(model.Email);
            user.Name = model.FullName;
            var result = await _userManager.UpdateAsync(user);
            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }
            return Ok();
        }
        /// <summary>
        /// get all users from my organization
        /// </summary>
        /// <returns></returns>
        [HttpGet("AllUsers")]
        [ProducesResponseType(200, Type = typeof(IQueryable<DtoOutUser>))]
        public IActionResult AllUsers()
        {
            var userId = User.Claims.First(c => c.Type == "UserID").Value;
            var users = this._organizationRepository.GetAllUsersFromMyOrganization(this._organizationRepository.GetOrganizationByUserId(userId).Id);
            var dtoOutUsers = new List<DtoOutUser>();
            foreach (var item in users)
            {
                dtoOutUsers.Add(_mapper.Map<ApplicationUser, DtoOutUser>(item));
            }

            return Ok(dtoOutUsers.AsQueryable());
        }
        /// <summary>
        /// get info about my organization
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetOrganizationInfo")]
        [ProducesResponseType(200, Type = typeof(DtoOutOrganization))]
        public IActionResult GetOrganizationInfo()
        {
            var userId = User.Claims.First(c => c.Type == "UserID").Value;
            var organization = this._organizationRepository.GetOrganizationByUserId(userId);
            return Ok(_mapper.Map<Organization, DtoOutOrganization>(organization));
        }

        /// <summary>
        /// change info in my organization
        /// </summary>
        /// <param name="model">info organization which will be changed</param>
        /// <returns></returns>
        [HttpPut("ChangeOrganizationInfo")]
        [ProducesResponseType(200, Type = typeof(void))]
        public async Task<IActionResult> ChangeOrganizationInfo(DtoInOrganization model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var userId = User.Claims.First(c => c.Type == "UserID").Value;

            var organization = this._organizationRepository.GetOrganizationByUserId(userId);
            organization.City = model.City;
            organization.EmployerIdentificationNumber = model.EmployerIdentificationNumber;
            organization.Name = model.Name;
            organization.Phone = model.Phone;
            organization.Street = model.Street;
            organization.ZipCode = model.ZipCode;
            organization.UpdatedAt = System.DateTimeOffset.UtcNow;
            organization.BankAccount = model.BankAccount;
            if (this._organizationRepository.UpdateOrganization(organization) == false)
            {
                return StatusCode(500, "Updatovani organizace se nepovedlo");
            }
            return Ok();

        }
        /// <summary>
        /// register organization with new user
        /// </summary>
        /// <param name="model">organization with user</param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("RegisterOrganization")]
        [ProducesResponseType(200, Type = typeof(void))]

        public async Task<IActionResult> RegisterOrganization(DtoInRegisterOrganization model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var organization = new Organization() { EmployerIdentificationNumber = model.EmployerIdentificationNumber, City = model.City, Name = model.OrganizationName, Phone = model.Phone, Street = model.Street, ZipCode = model.ZipCode, BankAccount = model.BankAccount };
            organization.Users = new List<ApplicationUser>();
            var user = new ApplicationUser() { Email = model.Email, UserName = model.Email, Name = model.UserName, Organization = organization };

            IdentityResult result = await _userManager.CreateAsync(user, model.Password);
            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }
            return Ok();
        }
        private IActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return StatusCode(500);
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError("", error.Description);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }
    }
}