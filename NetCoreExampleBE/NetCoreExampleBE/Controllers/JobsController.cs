﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace NetCoreExampleBE.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JobsController : ControllerBase
    {
        /// <summary>
        /// Example get method that return value that you passing in.
        /// </summary>
        /// <param name="exampleValue">Value that will be returned</param>
        /// <returns></returns>
        [HttpGet("{exampleValue}")]
        [AllowAnonymous]
        public IActionResult GetExampleMethod(string exampleValue)
        {
            return Ok(exampleValue);
        }
        [HttpGet("authorized/{exampleValue}")]
        [Authorize]
        public IActionResult GetAuthorizedExampleMethod(string exampleValue)
        {
            return Ok(exampleValue);
        }
    }
}