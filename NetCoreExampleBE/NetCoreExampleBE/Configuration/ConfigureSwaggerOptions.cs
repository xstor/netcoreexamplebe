﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace NetCoreExampleBE.Configuration
{
    public class ConfigureSwaggerOptions : IConfigureOptions<SwaggerGenOptions>
    {

        public void Configure(SwaggerGenOptions options)
        {
            options.SwaggerDoc("NetCoreExampleBE", new Microsoft.OpenApi.Models.OpenApiInfo()
            {
                Title = "NetCoreExampleBE",
                Version = "1",
                Description = ".Net Core examle of WEB API for LarvaSystems",
                Contact = new Microsoft.OpenApi.Models.OpenApiContact()
                {
                    Email = "vojtech.stor@larvasystems.cz",
                    Name = "LarvaSystems",
                    Url = new Uri("https://www.larvasystems.cz")
                },
                License = new Microsoft.OpenApi.Models.OpenApiLicense()
                {
                    Name = "MIT License",
                    Url = new Uri("https://en.wikipedia.org/wiki/MIT_License")
                },
            });
           /*
                 options.AddSecurityRequirement(new Microsoft.OpenApi.Models.OpenApiSecurityRequirement() {
                     {
                         new OpenApiSecurityScheme
                         {
                             Reference= new OpenApiReference
                             {
                                 Type=ReferenceType.SecurityScheme,
                                 Id="Bearer"
                             },
                             Scheme="oauth2",
                             Name="Bearer",
                             In=ParameterLocation.Header,
                         },
                         new List<string>()
                     }
                 });*/
            options.OperationFilter<AddAuthHeaderOperationFilter>();
            options.AddSecurityDefinition("Bearer", new Microsoft.OpenApi.Models.OpenApiSecurityScheme
            {
                Description = "JWT Authorization header using the Bearer Scheme. \r\n\r\n enter 'Bearer' [space] and then the token",
                Name = "Authorization",
                In = Microsoft.OpenApi.Models.ParameterLocation.Header,
                Type = Microsoft.OpenApi.Models.SecuritySchemeType.ApiKey,
                Scheme = "Bearer"
            });
            var xmlComentFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            var xmlComentsFullPath = Path.Combine(AppContext.BaseDirectory, xmlComentFile);
            options.IncludeXmlComments(xmlComentsFullPath); // aby tohle fungovalo musím kliknout na project properties a tam nastavit že chci povolit xml dokumentaci
        }
    }
}
