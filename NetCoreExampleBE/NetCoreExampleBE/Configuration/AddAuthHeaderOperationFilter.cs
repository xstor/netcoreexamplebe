﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace NetCoreExampleBE.Configuration
{
    public class AddAuthHeaderOperationFilter : IOperationFilter
    {
        private bool HasAttribute(MethodInfo methodInfo, Type type, bool inherit)
        {
            // inhertit = true also checks inherited attributes
            var actionAttributes = methodInfo.GetCustomAttributes(inherit);
            var controllerAttributes = methodInfo.DeclaringType.GetTypeInfo().GetCustomAttributes(inherit);
            var actionAndControllerAttributes = actionAttributes.Union(controllerAttributes);

            return actionAndControllerAttributes.Any(attr => attr.GetType() == type);
        }
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            var filterDescriptor = context.ApiDescription.ActionDescriptor.FilterDescriptors;
            var isAuthorized = HasAttribute(context.MethodInfo, typeof(AuthorizeAttribute), true);
            var allowAnonymous = HasAttribute(context.MethodInfo, typeof(AllowAnonymousAttribute), true);

            if (isAuthorized && !allowAnonymous)
            {

              

                operation.Security = new List<OpenApiSecurityRequirement>();

                //Add JWT bearer type
                operation.Security.Add(new OpenApiSecurityRequirement
                {

                   {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            },
                            Scheme = "oauth2",
                            Name = "Bearer",
                            In = ParameterLocation.Header,

                        },
                        new List<string>()
                    }
                });
            }
        }
    }
}
