﻿using AutoMapper;
using NetCoreExampleBE.Dtos.Out;
using NetCoreExampleBE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetCoreExampleBE.Mapper
{
    public class Mappings : Profile
    {
        public Mappings()
        {
            CreateMap<DtoOutUser, ApplicationUser>().ReverseMap();
            CreateMap<DtoOutOrganization, Organization>().ReverseMap();
        }
    }
}
