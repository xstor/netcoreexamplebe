﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetCoreExampleBE.Repository.IRepository
{
    public interface IRepository
    {
        bool Save();
    }
}
