﻿using NetCoreExampleBE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetCoreExampleBE.Repository.IRepository
{
    public interface IOrganizationRepository : IRepository
    {
        Organization GetOrganizationById(int id);

        Organization GetOrganizationByUserId(string userId);

        IList<ApplicationUser> GetAllUsersFromMyOrganization(int organizationId);
        bool CreateOrganization(Organization organization);

        bool UserWithEmailExistsInOrganization(string email, int organizationId);

        bool UpdateOrganization(Organization organization);

    }
}
