﻿using Microsoft.EntityFrameworkCore;
using NetCoreExampleBE.Data;
using NetCoreExampleBE.Models;
using NetCoreExampleBE.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NetCoreExampleBE.Repository
{
    public class OrganizationRepository : IOrganizationRepository
    {
        private readonly ApplicationDbContext _db;
        public OrganizationRepository(ApplicationDbContext db)
        {
            this._db = db;
        }
        public bool CreateOrganization(Organization organization)
        {
            this._db.Add(organization);
            return Save();
        }

        public Organization GetOrganizationById(int id)
        {
            return this._db.Organizations.Find(id);
        }

        public Organization GetOrganizationByUserId(string userId)
        {
            return this._db.Users.Include(x => x.Organization).Where(x => x.Id == userId).SingleOrDefault().Organization; //TODO vyzkoušet
        }
        public IList<ApplicationUser> GetAllUsersFromMyOrganization(int organizationId)
        {
            return this._db.Organizations.Include(y => y.Users).Where(x => x.Id == organizationId).SingleOrDefault().Users;
        }

        public bool Save()
        {
            return this._db.SaveChanges() >= 0 ? true : false;
        }

        public bool UpdateOrganization(Organization organization)
        {
            this._db.Organizations.Update(organization);
            return this.Save();
        }

        public bool UserWithEmailExistsInOrganization(string email, int organizationId)
        {

            if (this._db.Organizations.Include(x=>x.Users).Where(x => x.Id == organizationId).SingleOrDefault().Users.Where(x => x.Email == email).Count() != 0)
            {
                return true;
            }
            return false;
        }
    }
}
