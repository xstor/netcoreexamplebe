﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NetCoreExampleBE.Dtos.In
{
    public class DtoInForgotPassword
    {
        [Required]
        public string email { get; set; }
        [Required]
        public string token { get; set; }
        [Required]
        public string newPassword { get; set; }
    }
}
