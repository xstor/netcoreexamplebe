﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NetCoreExampleBE.Dtos.In
{
    public class DtoInUserChangeInfo
    {
        [Required]
        public string FullName { get; set; }

        public string Email { get; set; }
    }
}
